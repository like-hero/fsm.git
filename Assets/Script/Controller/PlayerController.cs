﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using M_FSM;

public class PlayerController : MonoBehaviour
{
    private M_FSMSystem m_fsm;
    private void Awake()
    {
        m_fsm = new M_FSMSystem();

        M_FSMState standState = new M_StandState(m_fsm, transform);
        standState.AddTransition(Transition.UpArrow, StateID.Jump);
        standState.AddTransition(Transition.DownArrow, StateID.Crouch);

        M_FSMState crouchState = new M_CrouchState(m_fsm, transform);
        crouchState.AddTransition(Transition.UpArrow, StateID.Stand);

        M_FSMState jumpState = new M_JumpState(m_fsm, transform);
        jumpState.AddTransition(Transition.DownArrow, StateID.Stand);

        m_fsm.AddState(standState);
        m_fsm.AddState(crouchState);
        m_fsm.AddState(jumpState);

        m_fsm.SetNormalState(standState);
    }
    private void Update()
    {
        m_fsm.Update();
    }
}