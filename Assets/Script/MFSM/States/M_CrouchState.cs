﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace M_FSM
{
    public class M_CrouchState : M_FSMState
    {
        public M_CrouchState(M_FSMSystem _fSM, Transform _ower) : base(_fSM, StateID.Crouch, _ower) { }

        public override void OnEnter() { }

        public override void OnExit() { }

        public override void OnReason()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                fSM.TransitionState(Transition.UpArrow);
            }
        }

        public override void OnUpdate()
        {
            Debug.Log("Crouch");
        }
    }
}
