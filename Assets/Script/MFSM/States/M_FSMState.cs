﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace M_FSM
{
    /// <summary>
    /// 状态转换条件枚举
    /// </summary>
    public enum Transition
    {
        NullTransition,
        UpArrow,
        DownArrow,
    }
    /// <summary>
    /// 状态枚举
    /// </summary>
    public enum StateID
    {
        Null,
        Stand,//站立
        Jump,//跳跃
        Crouch,//下蹲
    }
    public abstract class M_FSMState
    {
        protected M_FSMSystem fSM;
        protected Transform ower;
        protected Dictionary<Transition, StateID> dic;
        public StateID stateID { get; protected set; }
        /// <summary>
        /// 进入状态时调用
        /// </summary>
        public abstract void OnEnter();
        /// /// <summary>
        /// 状态持续时调用（当前状态执行的逻辑放在这里）
        /// </summary>
        public abstract void OnUpdate();
        /// <summary>
        /// 状态持续时调用（状态转换条件判断放在这里）
        /// </summary>
        public abstract void OnReason();
        /// <summary>
        /// 退出状态时调用
        /// </summary>
        public abstract void OnExit();
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="_fSM">FSM管理者</param>
        /// <param name="_stateID">当前状态ID</param>
        /// <param name="_ower">当前状态属于哪一个对象</param>
        public M_FSMState(M_FSMSystem _fSM, StateID _stateID, Transform _ower)
        {
            fSM = _fSM;
            stateID = _stateID;
            ower = _ower;
            dic = new Dictionary<Transition, StateID>();
        }
        /// <summary>
        /// 增加过渡条件
        /// </summary>
        /// <param name="transition">过渡条件</param>
        /// <param name="nextStateID">下一个状态</param>
        public bool AddTransition(Transition transition, StateID nextStateID)
        {
            if (transition == Transition.NullTransition || dic.ContainsKey(transition))
            {
                return false;
            }
            dic.Add(transition, nextStateID);
            return true;
        }
        /// <summary>
        /// 删除过渡条件
        /// </summary>
        /// <param name="transition">过渡条件</param>
        public bool DeleteTransition(Transition transition)
        {
            if (transition == Transition.NullTransition || !dic.ContainsKey(transition))
            {
                return false;
            }
            dic.Remove(transition);
            return true;
        }
        /// <summary>
        /// 获取过渡条件指向的下一个状态
        /// </summary>
        /// <param name="transition">过渡条件</param>
        /// <returns>下一个状态</returns>
        public StateID GetNextState(Transition transition)
        {
            if (transition == Transition.NullTransition || !dic.ContainsKey(transition))
            {
                return StateID.Null;
            }
            return dic[transition];
        }
    }
}
