﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace M_FSM
{
    public class M_StandState : M_FSMState
    {
        public M_StandState(M_FSMSystem _fSM, Transform _ower) : base(_fSM, StateID.Stand, _ower) { }

        public override void OnEnter() { }

        public override void OnExit() { }

        public override void OnReason()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                fSM.TransitionState(Transition.UpArrow);
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                fSM.TransitionState(Transition.DownArrow);
            }
        }

        public override void OnUpdate()
        {
            Debug.Log("Stand");
        }
    }
}
